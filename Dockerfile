ARG RUST_VERSION=1.53.0

FROM lukemathwalker/cargo-chef:latest-rust-$RUST_VERSION as planner
WORKDIR /code
COPY . .
RUN cargo chef prepare  --recipe-path /code/recipe.json

FROM lukemathwalker/cargo-chef:latest-rust-$RUST_VERSION as cacher
WORKDIR /code
COPY --from=planner /code/recipe.json recipe.json
RUN cargo chef cook --recipe-path recipe.json

FROM rust:$RUST_VERSION as test-builder

# Choose a workdir
WORKDIR /code

RUN apt-get update
RUN apt-get install -y libssl-dev pkg-config

ADD . .

COPY --from=cacher /code/target target
COPY --from=cacher /usr/local/cargo /usr/local/cargo

RUN cargo test --no-run --locked
